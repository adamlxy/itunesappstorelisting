import { FETCH_TOP_GROSSING_APP_REQUEST, FETCH_TOP_GROSSING_APP_DONE } from 'constants/ActionTypes'

const appRecommendation = (state = { list: [], keyword: '', isLoading: true }, action) => {
	switch (action.type) {
		case FETCH_TOP_GROSSING_APP_REQUEST:
			return { ...state, isLoading: true }
		case FETCH_TOP_GROSSING_APP_DONE:
			if (action.error) return { ...state, error: action.error }
			return { ...state, list: action.payload, isLoading: false }
		default:
			return state
	}
}

export default appRecommendation
