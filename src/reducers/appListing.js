import {
	FETCH_TOP_FREE_APP_REQUEST,
	FETCH_TOP_FREE_APP_DETAIL_REQUEST,
	FETCH_TOP_FREE_APP_DETAIL_DONE,
	FETCH_TOP_FREE_ALL_APP_DONE,
	FETCH_TOP_FREE_APP_DONE,
	INCREASE_TOP_FREE_LIST_PAGE,
	SCROLL_TOP_FREE_APP_END,
} from 'constants/ActionTypes'

const appListing = (state = { page: 0, list: [], isLoading: true, isScrollerEnd: false }, action) => {
	switch (action.type) {
		case FETCH_TOP_FREE_APP_REQUEST:
			return { ...state, isLoading: true }
		case FETCH_TOP_FREE_ALL_APP_DONE:
			if (action.error) return { ...state, error: action.error }
			return { ...state, appDB: action.payload, isLoading: false }
		case FETCH_TOP_FREE_APP_DONE: {
			const newList = [...state.list];
			return { ...state, list: newList.concat(action.payload), isLoading: false }
		}
		case FETCH_TOP_FREE_APP_DETAIL_REQUEST:
			return { ...state }
		case SCROLL_TOP_FREE_APP_END:
			return { ...state, isScrollerEnd: true }
		case FETCH_TOP_FREE_APP_DETAIL_DONE: {
			const { averageUserRating = 0, userRatingCount = 0 } = action.payload
			// ref: https://github.com/reduxjs/redux/blob/master/docs/recipes/reducers/ImmutableUpdatePatterns.md
			const newList = state.list.map((item, index) => {
				if (index !== action.appIndex) {
					return item
				}

				return {
					...item,
					rating: String(averageUserRating),
					comments: String(userRatingCount),
					isLoading: false,
				}
			})

			return { ...state, list: newList }
		}
		case INCREASE_TOP_FREE_LIST_PAGE:
			return { ...state, page: state.page + 1 }
		default:
			return state
	}
}

export default appListing
