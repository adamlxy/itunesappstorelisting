import { SHOW_NETWORK_ERROR } from 'constants/ActionTypes'

const notification = (state = { network_status: { hasError: false } }, action) => {
	switch (action.type) {
		case SHOW_NETWORK_ERROR:
			return { ...state, network_status: { hasError: action.hasError } }
		default:
			return state
	}
}

export default notification
