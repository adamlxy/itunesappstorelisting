import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from 'reducers'

import sagas from '../sagas'

const sagaMiddleware = createSagaMiddleware()

const middlewares = [sagaMiddleware]
const enhancer = [applyMiddleware(...middlewares)]

export default function configureStore(initialState = {}) {
	const store = createStore(rootReducer, initialState, ...enhancer)
	sagaMiddleware.run(sagas)
	return store
}
