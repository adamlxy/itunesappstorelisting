import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled, { ThemeProvider } from 'styled-components'
// import { throttle } from 'lodash'

import theme from 'theme'
import Scroller from 'components/Scroller'
import Toast from 'components/Toast'

import AppSearch from './AppSearch'
import AppRecommendation from './AppRecommendation'

import AppListing from './AppListing'

const Main = styled.div`
	font-family: ${(props) => props.theme.primaryFont};
	max-width: 640px;
	margin: auto;
`

class MainPage extends PureComponent {
	static propTypes = {
		fetchTopFreeApp: PropTypes.func,
		hasSearchKeyword: PropTypes.bool,
		isScrollerEnd: PropTypes.bool,
		isLoading: PropTypes.bool,
		hasNetworkError: PropTypes.bool,
	}

	static defaultProps = {
		fetchTopFreeApp: () => {},
		hasSearchKeyword: false,
		isScrollerEnd: false,
		isLoading: true,
		hasNetworkError: false,
	}

	componentDidMount() {
		document.title = "iTunes App Listing"
		this.props.fetchTopFreeApp()
	}

	// throttledFetchTopFreeApp = throttle(this.props.fetchTopFreeApp, 500, { trailing: false, leading: true })

	handleScroll = () => {
		const { isLoading, hasSearchKeyword, isScrollerEnd } = this.props

		// disable the scroll handler if typed keyoword at the searchBar or isLoading or scrolled 100th app
		if (isScrollerEnd || hasSearchKeyword || isLoading) {
			return
		}

		if (this.sb) {
			const { scrollHeight, offsetHeight, scrollTop } = this.sb
			if (scrollHeight - (offsetHeight + scrollTop) < 50) {
				// this.throttledFetchTopFreeApp()
				this.props.fetchTopFreeApp()
			}
		}
	}

	render() {
		return (
			<ThemeProvider theme={theme}>
				<Main>
					<AppSearch />
					<Scroller
						onScroll={this.handleScroll}
						innerRef={(sb) => {
							this.sb = sb
						}}
					>
						<AppRecommendation />
						<AppListing />
					</Scroller>
					<Toast isShow={this.props.hasNetworkError} />
				</Main>
			</ThemeProvider>
		)
	}
}

export default MainPage
