import { connect } from 'react-redux'

import { fetchTopGrossingApp } from 'actions/appRecommendation'

import { getAppByFilter } from 'selectors/appRecommendation'

import AppRecommendation from './AppRecommendation'

const mapStateToProps = (state) => {
	return {
		items: getAppByFilter(state),
		isLoading: state.appRecommendation.isLoading,
	}
}

const mapDispatchToProps = {
	fetchTopGrossingApp,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(AppRecommendation)
