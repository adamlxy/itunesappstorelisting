import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import Loader from 'components/Loader'
import NoResults from 'components/NoResults'
import AppRec from 'components/AppRec'

class AppRecommendation extends PureComponent {
	static propTypes = {
		items: PropTypes.arrayOf(PropTypes.object),
		isLoading: PropTypes.bool,
		fetchTopGrossingApp: PropTypes.func,
	}

	static defaultProps = {
		items: [],
		isLoading: true,
		fetchTopGrossingApp: () => {},
	}

	componentDidMount() {
		this.props.fetchTopGrossingApp()
	}

	render() {
		const { items, isLoading } = this.props

		return (
			<AppRec>
				<AppRec.Header>
					<AppRec.Title>Recommendation</AppRec.Title>
				</AppRec.Header>
				<AppRec.AppListBox>
					{isLoading && <Loader />}
					{!isLoading && items.length === 0 && <NoResults msg={'No results for top grossing app...'} />}
					{items.map((item) => (
						<AppRec.AppItem key={item.id}>
							<AppRec.AppImage isImageLarge isBorderCorner url={item.image} />
							<AppRec.AppTitle title={item.title}>{item.title}</AppRec.AppTitle>
							<AppRec.AppCategory>{item.category}</AppRec.AppCategory>
						</AppRec.AppItem>
					))}
				</AppRec.AppListBox>
			</AppRec>
		)
	}
}

export default AppRecommendation
