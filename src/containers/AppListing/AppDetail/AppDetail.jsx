import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import AppList from 'components/AppList'

class AppListing extends PureComponent {
	static propTypes = {
		fetchTopFreeAppDetail: PropTypes.func,
		appIndex: PropTypes.number.isRequired,
		item: PropTypes.shape({
			image: PropTypes.string,
			title: PropTypes.string,
			category: PropTypes.string,
			rating: PropTypes.string,
			comments: PropTypes.string,
			isLoading: PropTypes.bool,
			id: PropTypes.string,
		}).isRequired,
	}

	static defaultProps = {
		fetchTopFreeAppDetail: () => {},
	}

	componentDidMount() {
		// Bugfix: if app detail loaded, should not fecth app data again
		if (this.props.item.isLoading) {
			this.props.fetchTopFreeAppDetail(this.props.item.id, this.props.appIndex)
		}
	}

	render() {
		const { appIndex, item } = this.props
		// console.log("render==<>", this.props.item.rating)
		return (
			<AppList.AppItem
				appIndex={appIndex}
				isLoading={item.isLoading}
				image={item.image}
				title={item.title}
				category={item.category}
				rating={item.rating}
				comments={`(${item.comments})`}
			/>
		)
	}
}

export default AppListing
