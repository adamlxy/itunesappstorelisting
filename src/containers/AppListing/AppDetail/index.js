import { connect } from 'react-redux'
import { fetchTopFreeAppDetail } from 'actions/appListing'

import AppDetail from './AppDetail'

const mapStateToProps = (state, ownProps) => {
	return {
		item: ownProps.item,
		appIndex: ownProps.appIndex,
	}
}

const mapDispatchToProps = {
	fetchTopFreeAppDetail,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(AppDetail)
