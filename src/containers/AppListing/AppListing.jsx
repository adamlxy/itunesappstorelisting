import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import AppList from 'components/AppList'
import Loader from 'components/Loader'
import NoResults from 'components/NoResults'
import { ScrollerTrainling } from 'components/Scroller'

import AppDetail from './AppDetail'

class AppListing extends PureComponent {
	static propTypes = {
		items: PropTypes.arrayOf(PropTypes.object).isRequired,
		isLoading: PropTypes.bool.isRequired,
		hasSearchKeyword: PropTypes.bool.isRequired,
		isScrollerEnd: PropTypes.bool.isRequired,
	}

	render() {
		const { items, isLoading, isScrollerEnd, hasSearchKeyword } = this.props

		if (!isLoading && items.length === 0 && hasSearchKeyword) {
			return (
				<AppList>
					<NoResults msg={'No results for top free app...'} />
				</AppList>
			)
		}

		return (
			<AppList>
				{items.map((item, i) => (
					<AppDetail appIndex={i} key={item.id} item={item} />
				))}
				{isLoading && !isScrollerEnd && <Loader />}
				<ScrollerTrainling isShow={isScrollerEnd && !hasSearchKeyword}>
					No more app can be loaded....
				</ScrollerTrainling>
			</AppList>
		)
	}
}

export default AppListing
