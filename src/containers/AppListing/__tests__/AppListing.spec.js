import React from 'react'
import { shallow } from 'enzyme'

import AppListing from '../AppListing'

describe('AppListing', () => {
	it('should render no result', () => {
		const props = {
			isLoading: false,
			items: [],
			hasSearchKeyword: true,
			isScrollerEnd: false,
		}
		const wrapper = shallow(<AppListing {...props} />)
		expect(wrapper).toMatchSnapshot()
	})

	it('should render "No more app can be loaded"', () => {
		const props = {
			isLoading: false,
			items: [{ id: 'app-id' }],
			hasSearchKeyword: false,
			isScrollerEnd: true,
		}
		const wrapper = shallow(<AppListing {...props} />)
		expect(wrapper).toMatchSnapshot()
	})
})
