import { connect } from 'react-redux'

import { getAppByFilter } from 'selectors/appListing'

import AppListing from './AppListing'

const mapStateToProps = (state) => ({
	items: getAppByFilter(state),
	isLoading: state.appListing.isLoading,
	hasSearchKeyword: Boolean(state.appSearch.keyword),
	isScrollerEnd: state.appListing.isScrollerEnd, // scrolled to 100th app
})

const mapDispatchToProps = {}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(AppListing)
