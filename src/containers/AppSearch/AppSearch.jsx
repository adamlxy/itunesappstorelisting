import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import { debounce } from 'lodash'

import { SearchBox, SearchBar } from 'components/SearchBar'

const SEARCH_BAR_PLACEHOLDER = '🔍Search'

class Search extends PureComponent {
	static propTypes = {
		searchApp: PropTypes.func,
	}

	static defaultProps = {
		searchApp: () => {},
	}

	constructor(props) {
		super(props)

		this.state = {
			searchBarPlaceholder: SEARCH_BAR_PLACEHOLDER,
		}
	}

	debouncedSearchApp = debounce(this.props.searchApp, 300, { trailing: true, leading: true })

	handleOnChange = (e) => {
		this.debouncedSearchApp(e.target.value)
	}

	handleOnFocus = () => {
		this.setState({ searchBarPlaceholder: '' })
	}

	handleOnBlur = () => {
		this.setState({ searchBarPlaceholder: SEARCH_BAR_PLACEHOLDER })
	}

	render() {
		return (
			<SearchBox>
				<SearchBar
					type="text"
					placeholder={this.state.searchBarPlaceholder}
					onChange={this.handleOnChange}
					onFocus={this.handleOnFocus}
					onBlur={this.handleOnBlur}
				/>
			</SearchBox>
		)
	}
}

export default Search
