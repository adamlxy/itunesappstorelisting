import { connect } from 'react-redux'
import { fetchTopFreeApp } from 'actions/appListing'

import Main from './Main'

const mapStateToProps = (state) => ({
	isScrollerEnd: state.appListing.isScrollerEnd, // scrolled to 100th app
	isLoading: state.appListing.isLoading,
	hasSearchKeyword: Boolean(state.appSearch.keyword),
	hasNetworkError: state.notification.network_status.hasError,
})

const mapDispatchToProps = {
	fetchTopFreeApp,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Main)
