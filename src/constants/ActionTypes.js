// Counter
export const INCREMENT_COUNTER = 'INCREMENT_COUNTER'
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER'

export const SEARCH_APP = 'SEARCH_APP'
export const SHOW_NETWORK_ERROR = 'SHOW_NETWORK_ERROR'

export const INCREASE_TOP_FREE_LIST_PAGE = 'INCREASE_TOP_FREE_LIST_PAGE'
export const FETCH_TOP_FREE_APP_REQUEST = 'FETCH_TOP_FREE_APP_REQUEST'
export const FETCH_TOP_FREE_ALL_APP_DONE = 'FETCH_TOP_FREE_ALL_APP_DONE'
export const FETCH_TOP_FREE_APP_DONE = 'FETCH_TOP_FREE_APP_DONE'

export const FETCH_TOP_FREE_APP_DETAIL_REQUEST = 'FETCH_TOP_FREE_APP_DETAIL_REQUEST'
export const FETCH_TOP_FREE_APP_DETAIL_DONE = 'FETCH_TOP_FREE_APP_DETAIL_DONE'
export const SCROLL_TOP_FREE_APP_END = 'SCROLL_TOP_FREE_APP_END'

export const FETCH_TOP_GROSSING_APP_REQUEST = 'FETCH_TOP_GROSSING_APP_REQUEST'
export const FETCH_TOP_GROSSING_APP_DONE = 'FETCH_TOP_GROSSING_APP_DONE'
