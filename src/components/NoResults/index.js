import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const DEFAULT_MESSAGE = '🙄No results found for your keyword...'

const NoResultsWrapper = styled.div`
	font-size: ${(props) => props.theme.fontSizeL};
	margin: auto;
	color: ${(props) => props.theme.borderColor};
`

const NoResults = ({ msg }) => <NoResultsWrapper>{msg}</NoResultsWrapper>

NoResults.propTypes = {
	msg: PropTypes.string,
}

NoResults.defaultProps = {
	msg: DEFAULT_MESSAGE,
}

export default NoResults
