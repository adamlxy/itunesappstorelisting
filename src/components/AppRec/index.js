import styled, { keyframes } from 'styled-components'
import { fadeIn } from 'react-animations'

import { AppImage } from 'components/AppList'

const fadeInAnimation = keyframes`${fadeIn}`

const AppRec = styled.div`
	border-bottom: 1px solid ${(props) => props.theme.borderColor};
`

const Header = styled.div`
	width: 100%;
	padding-top: ${(props) => props.theme.spaceM};
	padding-left: ${(props) => props.theme.spaceM};
`

const Title = styled.span`
	font-size: ${(props) => props.theme.fontSizeL};
	font-weight: bold;
`

const AppListBox = styled.ul`
	list-style: none;
	margin: 0;
	padding: ${(props) => props.theme.spaceXS};
	display: flex;
	flex-direction: row;
	margin: ${(props) => props.theme.spaceM};

	li:not(:first-child) {
		margin-left: ${(props) => props.theme.spaceM};
	}

	overflow-x: auto;
`

const AppItem = styled.li`
	text-align: left;
	height: 150px;
	animation: 0.5s ${fadeInAnimation};
`

const AppTitle = styled.div`
	width: ${(props) => props.theme.imageSizeL};
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	font-size: ${(props) => props.theme.fontSizeS};
	margin-top: ${(props) => props.theme.spaceXS};
`

const AppCategory = styled.div`
	font-size: ${(props) => props.theme.fontSizeS};
	margin-top: ${(props) => props.theme.spaceS};
	color: ${(props) => props.theme.secondaryColor};
`

AppRec.Header = Header
AppRec.Title = Title
AppRec.AppListBox = AppListBox
AppRec.AppItem = AppItem
AppRec.AppImage = AppImage
AppRec.AppTitle = AppTitle
AppRec.AppCategory = AppCategory

export default AppRec
