import styled from 'styled-components'

export const SearchBox = styled.div`
	display: flex;
	height: 42px;
	background-color: #f8f8f9;
	text-align: center;
	border-bottom: 1px solid ${(props) => props.theme.borderColor};
	justify-content: center;
	align-items: center;
`

export const SearchBar = styled.input`
	width: 94%;
	height: 28px;
	border: 1px solid #f8f8f9;
	text-align: center;
	color: #${(props) => props.theme.primaryColor};
	font-size: ${(props) => props.theme.fontSizeS};
	line-height: 1;
	border-radius: 6px;
	cursor: pointer;
	background-color: #e4e5e6;

	&:focus {
		outline: none;
	}
`
