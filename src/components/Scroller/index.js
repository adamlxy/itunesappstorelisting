import styled from 'styled-components'

const Scroller = styled.div`
	height: 800px;
	overflow-y: auto;
	overflow-x: hidden;
`

export const ScrollerTrainling = styled.div`
	display: ${(props) => (props.isShow ? 'block' : 'none')};
	margin: auto;
	padding: ${(props) => props.theme.spaceS};
	font-size: ${(props) => props.theme.fontSizeL};
	color: ${(props) => props.theme.borderColor};
`

export default Scroller
