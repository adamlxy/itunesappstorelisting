export const filterHelper = (item, keys, keyword) =>
	keys.some((key) => (item[key] || '').toLowerCase().includes(keyword.toLowerCase()))
