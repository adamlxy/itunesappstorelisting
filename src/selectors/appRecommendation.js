import { createSelector } from 'reselect'

import { filterHelper } from 'utils/index'

import { getAppSearchKeyword } from './appSearch'

export const getAppRecommendationList = (state) => state.appRecommendation.list

export const getAppByFilter = createSelector([getAppSearchKeyword, getAppRecommendationList], (keyword, list) => {
	if (!keyword) {
		return list
	}

	return list.filter((item) => filterHelper(item, ['title', 'category', 'summary', 'artist'], keyword))
})
