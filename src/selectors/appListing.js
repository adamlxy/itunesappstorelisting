import { createSelector } from 'reselect'
import { filterHelper } from 'utils/index'

import { getAppSearchKeyword } from './appSearch'

export const getAppListingPage = (state) => state.appListing.page
export const getAppListingappDB = (state) => state.appListing.appDB

export const getAppListingList = (state) => state.appListing.list

export const getAppByFilter = createSelector([getAppSearchKeyword, getAppListingList], (keyword, list) => {
	if (!keyword) {
		return list
	}

	return list.filter((item) => filterHelper(item, ['title', 'category', 'artist'], keyword))
})
