import { getAppByFilter } from '../appListing'

describe('test getAppByFilter', () => {
	it('returns list itself if no keyword typed', () => {
		const state = {
			appSearch: {
				keyword: '',
			},
			appListing: {
				page: 1,
				list: [{ title: 'app name 1' }, { title: 'app name 2' }],
			},
		}
		expect(getAppByFilter(state)).toEqual(state.appListing.list)
	})

	it('returns filtered list by keyword', () => {
		const state = {
			appSearch: {
				keyword: 'Name 1',
			},
			appListing: {
				page: 1,
				list: [{ title: 'app name 1' }, { title: 'app name 2' }],
			},
		}
		expect(getAppByFilter(state)).toEqual([{ title: 'app name 1' }])
	})
})
