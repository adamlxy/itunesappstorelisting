import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import MainPage from 'containers'

function Routes() {
	return (
		<Router>
			<Route path="/" component={MainPage} />
		</Router>
	)
}

export default Routes
