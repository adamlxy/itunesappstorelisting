import { FETCH_TOP_GROSSING_APP_REQUEST } from 'constants/ActionTypes'

export function fetchTopGrossingApp() {
	return {
		type: FETCH_TOP_GROSSING_APP_REQUEST,
	}
}
