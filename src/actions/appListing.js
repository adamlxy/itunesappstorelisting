import {
	FETCH_TOP_FREE_APP_REQUEST,
	FETCH_TOP_FREE_APP_DETAIL_REQUEST,
	INCREASE_TOP_FREE_LIST_PAGE,
	SCROLL_TOP_FREE_APP_END,
} from 'constants/ActionTypes'

export function fetchTopFreeApp(page) {
	return {
		type: FETCH_TOP_FREE_APP_REQUEST,
		page,
	}
}

export function fetchTopFreeAppDetail(id, appIndex) {
	return {
		type: FETCH_TOP_FREE_APP_DETAIL_REQUEST,
		id,
		appIndex,
	}
}

export function scrollTopFreeAppEnd() {
	return {
		type: SCROLL_TOP_FREE_APP_END,
	}
}

export function increaseTopFeeAppPage() {
	return {
		type: INCREASE_TOP_FREE_LIST_PAGE,
	}
}
