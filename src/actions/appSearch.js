import { SEARCH_APP } from 'constants/ActionTypes'

export function searchApp(keyword) {
	return {
		type: SEARCH_APP,
		keyword,
	}
}
