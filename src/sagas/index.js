import { all, fork } from 'redux-saga/effects'
import appListing from './appListing'
import appRecommendation from './appRecommendation'

export default function* sagas() {
	yield all([fork(appListing)])
	yield all([fork(appRecommendation)])
}
