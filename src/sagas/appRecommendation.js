import { put, fork, takeLatest } from 'redux-saga/effects'
import axios from 'axios'

import { FETCH_TOP_GROSSING_APP_REQUEST, FETCH_TOP_GROSSING_APP_DONE, SHOW_NETWORK_ERROR } from 'constants/ActionTypes'

const delay = (ms) => new Promise((res) => setTimeout(res, ms))

export function* fetchFreeGrossingApp() {
	try {
		const res = yield axios.get('/api/topgrossingapplications.json')

		let payload = []
		if (res.status === 200) {
			payload = res.data
		} else {
			throw new Error('Something went wrong at api.')
		}

		payload = payload.map((item) => ({
			id: item.id.attributes['im:id'],
			category: item.category.attributes.label,
			image: item['im:image'][1].label,
			title: item['im:name'].label,
			summary: item.summary.label,
			artist: item['im:artist'].label,
		}))

		yield delay(100)

		yield put({ type: FETCH_TOP_GROSSING_APP_DONE, payload })
	} catch (error) {
		yield put({ type: SHOW_NETWORK_ERROR, hasError: true })
	}
}

export default function*() {
	yield fork(takeLatest, FETCH_TOP_GROSSING_APP_REQUEST, fetchFreeGrossingApp)
}
