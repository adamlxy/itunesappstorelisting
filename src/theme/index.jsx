const typographyConfig = {
	lineHeight: '1.5',
	fontTitle: '1.25rem',
	fontTitleL: '1.5rem',
	primaryFont: 'arial,sans-serif',
}

const imageConfig = {
	borderCorner: '20%',
	borderCircle: '50%',
	imageSizeS: '53px',
	imageSizeM: '75px',
	imageSizeL: '100px',
}

const spacingConfig = {
	spaceXS: '4px',
	spaceS: '8px',
	spaceM: '16px',
	spaceL: '24px',
	spaceXL: '32px',
	spaceXXL: '48px',
	spaceXXXL: '64px',
}

const fontSizeConfig = {
	fontSizeXS: '0.75rem',
	fontSizeS: '0.875rem',
	fontSizeM: '1rem',
	fontSizeL: '1.25rem',
	fontSizeXL: '1.5rem',
	fontSizeXXL: '2rem',
}

const colorsConfig = {
	primaryColor: '#1e1d1d',
	secondaryColor: '#a8a8a8',
	borderColor: '#e1e1e1',
}

export default {
	...typographyConfig,
	...imageConfig,
	...spacingConfig,
	...fontSizeConfig,
	...colorsConfig,
}
