// https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#running-tests

import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })
