const Koa = require('koa')
const axios = require('axios')
const logger = require('koa-logger')
const KoaRouter = require('koa-router')
const koaStatic = require('koa-static')
const path = require('path')
const fs = require('fs')
const os = require('os');

const app = new Koa()

const REGION = 'us'

const BUILD_VERSION_PATH = path.join(__dirname, '../BUILD_VERSION_NO');
const buildVersion = fs.existsSync(BUILD_VERSION_PATH) ? fs.readFileSync(BUILD_VERSION_PATH).toString() : 'unknown'

const router = new KoaRouter({
	prefix: '/api',
})

router.get('/healthcheck', async (ctx) => {
	ctx.body = {
		hostName: os.hostname(),
		buildVersion,
		memoryUsage: process.memoryUsage(),
	}
})

router.get('/topfreeapplications.json', async (ctx) => {
	const res = await axios.get(`https://itunes.apple.com/${REGION}/rss/topfreeapplications/limit=100/json`)
	ctx.body = res.data.feed.entry
})

router.get('/lookup/:appID.json', async (ctx) => {
	const appID = ctx.params.appID
	const res = await axios.get(`https://itunes.apple.com/hk/lookup?id=${appID}`)

	ctx.body = res.data.results[0] || {}
})

router.get('/topgrossingapplications.json', async (ctx) => {
	const res = await axios.get(`https://itunes.apple.com/${REGION}/rss/topgrossingapplications/limit=10/json`)
	ctx.body = res.data.feed.entry
})

app.use(logger())
app.use(router.routes())
app.use(koaStatic(path.join(__dirname, '../build')))

const port = process.env.PORT || 9000

app.listen(port, () => {
	console.info(`Service started on port ${port}`)
})
